--Gamestates/Config.lua
--by Fallstar

local Config = {}

local fullscreen = false

function Config:enter()
	Previous = "Gamestates/Menu"
end


function Config:loadUserConfig()
	if not love.filesystem.exists("miaou") then
		love.filesystem.write("miaou", "lvl: 1")
		Config.lvl = 1
	else
		for line in love.filesystem.lines("miaou") do
			var, arg = line:match("(.*): (.*)") -- Split following the "<KEY>: <VALUE>" schema
			if var == "lvl" then
				Config.lvl = tonumber(arg)
			end
		end
	end
end

function Config:update(dt)
	Gui.group.push{grow = "down", pos = {0.3*Screen.width,0.3*Screen.height}}  --Gui group for Button
	Gui.Label{ text = "Options"}
	if love.system.getOS() ~= "Android" then
		if Gui.Button{id = "Fullesreen", text="Plein écran", size = {0.4*Screen.width, 0.2*Screen.height}} then
			if fullscreen == true then
				love.window.setFullscreen(false, "desktop")
				fullscreen = false
			else
				fullscreen = true
				love.window.setFullscreen(true, "desktop")
			end
		end
	end
	Gui.group.pop{}
end
	

function Config:draw()
	Screen.width, Screen.height = love.window.getDimensions()
	Gui.core.draw() --Draw Gui
end
 
return Config
