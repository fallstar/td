--Gamestates/Level.lua
--by Fallstar

local Level = {}

local other = false

function Level:update(dt)
	Gui.group.push{grow = "down", pos = {0.3*Screen.width,0.1*Screen.height}}
	Gui.Label{ text = "Choix du niveau"}
	if not other then
		for i=1, Config.lvl, 1 do
			if Gui.Button{id = "lvl"..i, text = "Niveau "..i, size ={0.4*Screen.width, 0.2*Screen.height}} then
				Gamestate.switch(Game, false, i)
			end
		end
		if Gui.Button{id = "Miaou", text = "Autre", size ={0.4*Screen.width, 0.2*Screen.height}} then -- For non official levels
			other = true
		end
	else
		Gui.Label{text = "Bientôt !"}
		if Gui.Button{id = "back", text = "Retour", size ={0.4*Screen.width, 0.2*Screen.height}} then
			other = false
		end
	end
	Gui.group.pop{}
end

function Level:draw()
	Gui.core.draw() --Draw Gui
end

return Level
