--Gamestates/Game.lua
--by Fallstar

local Game = {}

function Game:init()
	bg_image = love.graphics.newImage("Ressources/Background.png")
	bg_image:setWrap("repeat", "repeat")
	bg_quad = love.graphics.newQuad(0, 0, Screen.width, Screen.height, bg_image:getWidth(), bg_image:getHeight())
	path_image = love.graphics.newImage("Ressources/Path.png")
	path_image:setWrap("repeat", "repeat")
end


function Game:enter(Level, resume, lvl)
	Previous = "Gamestates/Menu"
	Game.resume = resume
	love.graphics.setBackgroundColor(70, 70, 70)
	if not Game.resume then
		nbWave = 0
		Game.lvl = lvl
		Game:reset()
		Game:newWave()
	end
end

function Game:reset()
	foes = {}
	nbFoes = 2 --Initial number of foes
	won = false
	turrets = {}
	nbTurrets = 0
	bombs = {}
	nbBombs = 0
	snipers = {}
	nbSnipers = 0
	goos = {}
	nbGoos = 0
	hp = 3
	gold = 48
	shop = false
	nbWave = 0
	ready = false
	weapon = 1
	frame = 0
	drawPath = {}
end


function Game:path()
	local prevx = 0
	local prevy = 0.5*Screen.height
	for i, segment in ipairs(path) do
		if segment.d then
			if segment.d == "right" then
				table.insert(drawPath, {x=prevx, y=prevy-0.025*Screen.height, l=segment.l+0.025*Screen.width, w=0.05*Screen.height})
				prevx = prevx+segment.l
			elseif segment.d == "up" then
				table.insert(drawPath, {x=prevx-0.025*Screen.width, y=prevy, l=0.05*Screen.width, w=-(segment.l+0.025*Screen.height)})
				prevy = prevy-segment.l
			elseif segment.d == "down" then
				table.insert(drawPath, {x=prevx-0.025*Screen.width, y=prevy, l=0.05*Screen.width, w=segment.l+0.025*Screen.height})
				prevy = prevy+segment.l
			end
		else
			prevx = segment.x
			prevy = segment.y
		end
	end
end

function Game:newWave() -- Called at each new wave
	nbFoes = nbFoes+1
	nbWave = nbWave+1
	if nbWave <= 3 then
		gold= nbWave*2+gold
	else
		gold = gold+3
	end
	foes = {}
	path = require("Levels/Level"..Game.lvl)
	Game:path()
	for i=1, nbFoes do
		table.insert(foes, { x = -i*0.03*Screen.width, y = 0.5*Screen.height, hp = 30+10*2*nbWave, killer = false, fast = false, segment = 1, prevv = 0.5*Screen.height, prevh=0})

	end
	for i, foe in ipairs(foes) do
		if i % 2 == 0 then
			foe.hp = foe.hp + 50
		end
		if not (Game.lvl == 1) then
			if i % 3 == 0 then
				foe.fast = true
				foe.hp = foe.hp - 30
			end
			if secondPath then
				foe.segment = 9
				for j, segment in ipairs(path) do
					if segment.y then
						foe.y = segment.y
						foe.prevv = segment.y
						foe.prevh = segment.x
					end
				end
			end
			if i % 3 == 0 then
				secondPath = not secondPath
			end
		end
		if i % 5 == 0 then
			foe.killer = true
		end
	end
	won = false
	dead = false
	shop = true

end

function Game:shop()
	ready=false
 	Gui.group.push{grow = "down", pos = {0.3*Screen.width,0.3*Screen.height}}
	Gui.Label{text = "Échoppe"}
	if gold>=10 then
		if Gui.Button{id = "Turret1", text="Tourelle 1 (10po)", size = {0.4*Screen.width, 0.05*Screen.height}} then
 		      	nbTurrets=nbTurrets+1
			gold=gold-10
		end
	end
	if gold>=10 then
		if Gui.Button{id = "Goo", text = "Goo (10po)", size = {0.4*Screen.width, 0.05*Screen.height}} then
			nbGoos=nbGoos+1
			gold = gold-10
		end
	end
	if gold>=20 then
		if Gui.Button{id = "Turret2", text = "Bombe (20po)", size = {0.4*Screen.width, 0.05*Screen.height}} then
			nbBombs=nbBombs+1
			gold=gold-20
		end
	end
	if gold>=30 then
		if Gui.Button{id = "Repair", text = "Kit de réparation (30po)", size = {0.4*Screen.width, 0.05*Screen.height}} then
		for i, turret in ipairs(turrets) do
			if turret then
				turret.hp=100
			end
		end
		for i, bomb in ipairs(bombs) do
			if bomb then
				bomb.hp=200
			end
		end
		gold=gold-30
		end
	end
	if gold>=40 then
		if Gui.Button{id = "Turret3", text = "Sniper (40po)", size = {0.4*Screen.width, 0.05*Screen.height}} then
		nbSnipers=nbSnipers+1
		gold=gold-40
		end
	end
	if Gui.Button{id = "Leave", text = "Quitter", size = {0.4*Screen.width, 0.05*Screen.height}} then
	    shop=false
	end
     	Gui.group.pop{}
end

function Game:checkLocation(x, y)
	local ok = true
	if y+0.03*Screen.height >= 0.9*Screen.height then
		ok= false
	end
	if ok then
		for i, segment in ipairs(drawPath) do
			if (segment.w>=0 and x+0.03*Screen.width > segment.x and x-0.03*Screen.width < segment.x+segment.l and y+0.03*Screen.height > segment.y and y-0.03*Screen.height < segment.y+segment.w) or (segment.w<=0 and x+0.03*Screen.width > segment.x and x-0.03*Screen.width < segment.x+segment.l and y+0.03*Screen.height > segment.y+segment.w and y-0.03*Screen.height < segment.y) then
				ok = false
				break
			end
		end
	end
	if ok then
		for i, turret in ipairs(turrets) do
			if x+0.03*Screen.width > turret.x-0.03*Screen.width and x-0.03*Screen.width < turret.x+0.03*Screen.width and y+0.03*Screen.height > turret.y-0.03*Screen.height and y-0.03*Screen.height < turret.y+0.03*Screen.height then
				ok = false
				break
			end
		end
	end
	if ok then
		for i, bomb in ipairs(bombs) do
			if x+0.03*Screen.width > bomb.x-0.03*Screen.width and x-0.03*Screen.width < bomb.x+0.03*Screen.width and y+0.03*Screen.height > bomb.y-0.03*Screen.height and y-0.03*Screen.height < bomb.y+0.03*Screen.height then
				ok = false
				break
			end
		end
	end
	if ok then
		for i, sniper in ipairs(snipers) do
			if x+0.03*Screen.width > sniper.x-0.03*Screen.width and x-0.03*Screen.width < sniper.x+0.03*Screen.width and y+0.03*Screen.height > sniper.y-0.03*Screen.height and y-0.03*Screen.height < sniper.y+0.03*Screen.height then
				ok = false
				break
			end
		end
	end
	return ok
end


function Game:mousepressed(x, y, button)
	if not shop then
		if weapon==1 then
	       		if #turrets < nbTurrets then
				local out = Game:checkLocation(x, y)
				if out then
					table.insert(turrets, { x = x, y = y, hp = 100 })
				end
			end
		elseif weapon==2 then
			if #bombs < nbBombs then
				local out = Game:checkLocation(x, y)
				if out then
					table.insert(bombs, { x = x, y = y, hp = 200 })
				end
			end
		elseif weapon==3 then
			if #snipers < nbSnipers	then
				local out = Game:checkLocation(x, y)
				if out then
					table.insert(snipers, { x = x, y = y, hp = 100 })
				end
			end
		elseif weapon==4 then
			if #goos < nbGoos then
				local out = Game:checkLocation(x, y)
				if not out then
					table.insert(goos, {x = x, y = y, hp = 500 })
				end
			end
		end
	end
end

function Game:miaou(dt) -- Gotta find a better name.
	if not won and not dead then
		for i, foe in ipairs(foes) do
			for j, turret in ipairs(turrets) do
				if foe and turret and foe.x > turret.x-(0.063*Screen.width) and foe.x < turret.x+(0.063*Screen.width) and foe.y > turret.y-(0.1*Screen.height) and foe.y < turret.y+(0.1*Screen.height) and random==1 then
					if foe.killer then
						turret.hp = turret.hp-1*(800/Screen.width)*dt
						if turret.hp <= 0 then
							turrets[j]=false
						end
					end
					local strengh = math.random(0,2)
					foe.hp =foe.hp-0.5*(800/Screen.width)*strengh*60*dt
					if foe.hp <=0 then
						foes[i]=false
						gold=gold+1
					end
		        	end
			end
			for j, bomb in ipairs(bombs) do
				if foe and foe.x > bomb.x-(0.063*Screen.width) and foe.x < bomb.x+(0.063*Screen.width) and foe.y > bomb.y-(0.1*Screen.height) and foe.y < bomb.y+(0.1*Screen.height) and frame % 100 == 0 then
					if foe.killer then
						bomb.hp = bomb.hp-1*(800/Screen.width)*60*dt
						if bomb.hp <= 0 then
							bombs[j]=false
						end
					end
					foe.hp=foe.hp-25*(800/Screen.width)*60*dt

					if love.system.getOS() == "Android" then
						love.system.vibrate(0.1)
					end
						if foe.hp <=0 then
						foes[i]=false
						gold=gold+1
					end
				end
			end
			for j, sniper in pairs(snipers) do
				if foe and i % random == 0 and i%j == 0 and frame % 100 == 0 then
					foe.hp=foe.hp-10*(800/Screen.width)*60*dt
					if love.system.getOS() == "Android" then
						love.system.vibrate(0.05)
					end
					if foe.hp <= 0 then
						foes[i]=false
						gold=gold+1
					end
				end

			end

			if foe then --[Foe movment
				if path[foe.segment].d == "right" then
					if foe.x < foe.prevh+path[foe.segment].l then
						for j, goo in ipairs(goos) do
							if foe and foe.x > goo.x-0.025*Screen.width and foe.x < goo.x+0.025*Screen.width and foe.y > goo.y-0.025*Screen.height and foe.y < goo.y+0.025*Screen.height then
			 					foe.x = foe.x-0.1
							end
						end
						if foe.fast then
							foe.x = foe.x+(1.5/800)*Screen.width*60*dt
						else
							foe.x = foe.x+(1/800)*Screen.width*60*dt
						end
					else
						foe.prevh = foe.prevh+path[foe.segment].l
						foe.segment = foe.segment+1
					end
				elseif path[foe.segment].d == "up" then
					if foe.y > foe.prevv-path[foe.segment].l then
						if foe.fast then
							foe.y = foe.y-(1.5/600)*Screen.height*60*dt
						else
							foe.y = foe.y-(1/600)*Screen.height*60*dt
						end

					else
						foe.prevv = foe.prevv-path[foe.segment].l
						foe.segment = foe.segment+1
					end
				elseif path[foe.segment].d == "down" then
					if foe.y < foe.prevv+path[foe.segment].l then
						if foe.fast then
							foe.y = foe.y+(1.5/600)*Screen.height*60*dt
						else
							foe.y = foe.y+(1/600)*Screen.height*60*dt
						end
					else
						foe.prevv = foe.prevv+path[foe.segment].l
						foe.segment = foe.segment+1
					end
				end --]
				if foe.x>=Screen.width then
					hp=hp-foe.hp/100
					foes[i]=false
				end
			end
		end
	elseif dead then
		Gui.group.push{grow = "down", pos = {0.5*Screen.width,0.5*Screen.height}}
		Gui.Label{ text = "Sous-merde"}
		if Gui.Button{id = "Replay", text="Rejouer ?"} then
			Gamestate.switch(Level)
		end
		Gui.group.pop{}
	elseif won then
		Gui.group.push{grow = "down", pos = {0.45*Screen.width,0.01*Screen.height}}
		Gui.Label{ text = "Bravo"}
		if Gui.Button{id = "Continue", text="Continuer", size ={0.1*Screen.width, 0.05*Screen.height} } then
			Game:newWave()
		end
		Gui.group.pop{}
	end
end


function Game:tableSize(tab)
	local cpt=0
	for i, val in ipairs(tab) do
		if val then
			cpt=cpt+1
		end
	end
	return cpt
end


function Game:drawFoes()
	if not won then
		for i, foe in ipairs(foes) do
			if foe then
				if foe.killer then
					love.graphics.setColor(200,foe.hp,0,255) -- foe gets darker as its hp goes down.
					love.graphics.circle("fill", foe.x, foe.y, 0.01*Screen.width, 100) --shows foe
					love.graphics.print(foe.hp, foe.x, 0.7*Screen.height) --shows hp
				else
					love.graphics.setColor(0,foe.hp*2,foe.hp,255) -- foe gets darker as its hp goes down.
					love.graphics.circle("fill", foe.x, foe.y, 0.008*Screen.width, 100) --shows foe
					love.graphics.print(foe.hp, foe.x, 0.7*Screen.height) --shows hp
				end
			end
		end
	end
end

function Game:drawTurrets()
	for i, turret in ipairs(turrets) do
		if turret then
			love.graphics.setColor(255, 0, 0, turret.hp*2)

			love.graphics.rectangle("fill", turret.x-(0.03*Screen.width), turret.y-(0.03*Screen.height), 0.06*Screen.width, 0.06*Screen.height)
			love.graphics.setColor(255, 0, 0, turret.hp)
			love.graphics.rectangle("line", turret.x-(0.063*Screen.width), turret.y-(0.1*Screen.height), 0.126*Screen.width, 0.2*Screen.height)
		end
	end
	for i, bomb in ipairs(bombs) do
		love.graphics.setColor(200, 200, 200, bomb.hp)
		love.graphics.rectangle("fill",  bomb.x-(0.03*Screen.width), bomb.y-(0.03*Screen.height), 0.06*Screen.width, 0.06*Screen.height)
		love.graphics.setColor(200, 200, 200, bomb.hp/2)
		love.graphics.rectangle("line", bomb.x-(0.063*Screen.width), bomb.y-(0.1*Screen.height), 0.126*Screen.width, 0.2*Screen.height)
	end
	for i, sniper in ipairs(snipers) do
		love.graphics.setColor(0, 200, 50, 255)
		love.graphics.rectangle("fill",  sniper.x-(0.03*Screen.width), sniper.y-(0.03*Screen.height), 0.06*Screen.width, 0.06*Screen.height)
	end
	for i, goo in ipairs(goos) do
		love.graphics.setColor(0, 200, 50, 100)
		love.graphics.rectangle("fill",  goo.x-(0.025*Screen.width), goo.y-(0.025*Screen.height), 0.05*Screen.width, 0.05*Screen.height)
	end
	if not ready then
		if weapon==1 then
			if #turrets ~= nbTurrets then
				local x, y = love.mouse.getPosition(x, y)
				if y<0.9*Screen.height then
					love.mouse.setVisible(false)
					love.graphics.setColor(255,0,0, 100)
					love.graphics.rectangle("fill", x-0.03*Screen.width, y-0.03*Screen.height, 0.06*Screen.width, 0.06*Screen.height)
					love.graphics.setColor(255,0,0, 50)
					love.graphics.rectangle("fill", x-0.063*Screen.width, y-0.1*Screen.height, 0.126*Screen.width, 0.2*Screen.height)
				else
					love.mouse.setVisible(true)
				end
			else
				love.mouse.setVisible(true)
			end
		elseif weapon==2 then
			if #bombs ~= nbBombs then
				local x, y = love.mouse.getPosition(x, y)
				if y<0.9*Screen.height then
					love.mouse.setVisible(false)
					love.graphics.setColor(200,200,200, 100)
					love.graphics.rectangle("fill", x-0.03*Screen.width, y-0.03*Screen.height, 0.06*Screen.width, 0.06*Screen.height)
					love.graphics.setColor(200,200,200, 50)
					love.graphics.rectangle("fill", x-0.063*Screen.width, y-0.1*Screen.height, 0.126*Screen.width, 0.2*Screen.height)
				else
					love.mouse.setVisible(true)
				end
			else
				love.mouse.setVisible(true)
			end
		elseif weapon==3 then
			if #snipers ~= nbSnipers then
				local x, y = love.mouse.getPosition(x, y)
				love.mouse.setVisible(true)
				if y<0.9*Screen.height then
					love.mouse.setVisible(false)
					love.graphics.setColor(0,200,50, 100)
					love.graphics.rectangle("fill", x-0.03*Screen.width, y-0.03*Screen.height, 0.06*Screen.width, 0.06*Screen.height)
				else
					love.mouse.setVisible(true)
				end
			else
				love.mouse.setVisible(true)
			end
		elseif weapon==4 then
			if #goos ~= nbGoos then
				local x, y = love.mouse.getPosition(x, y)
				love.mouse.setVisible(true)
				if y<0.9*Screen.height then
					love.mouse.setVisible(false)
					love.graphics.setColor(0,200,0, 50)
					love.graphics.rectangle("fill", x-0.025*Screen.width, y-0.025*Screen.height, 0.05*Screen.width, 0.05*Screen.height)
				else
					love.mouse.setVisible(true)
				end
			else
				love.mouse.setVisible(true)
			end
		else
			love.mouse.setVisible(true)
		end
	else
		love.mouse.setVisible(true)
	end
end



function Game:drawPath()
	love.graphics.setColor(20, 70, 200)
	for i, segment in ipairs(drawPath) do
		path_quad = love.graphics.newQuad(segment.l, segment.w, segment.l, segment.w,  path_image:getWidth(), path_image:getHeight())
		love.graphics.draw(path_image, path_quad, segment.x, segment.y)
		--love.graphics.rectangle("fill", segment.x, segment.y, segment.l, segment.w)
	end
end



function Game:menu()
	Gui.group.push{grow = "right", pos = {0.4*Screen.width,0.925*Screen.height}}
	if Gui.Button{id = "Weapon1", text = "Tourelle", size = {0.1*Screen.width, 0.05*Screen.height}} then
		weapon=1
	end
	if Gui.Button{id = "Weapon2", text = "Bombe", size = {0.1*Screen.width, 0.05*Screen.height}} then
		weapon=2
	end
	if Gui.Button{id = "Weapon3", text = "Sniper", size = {0.1*Screen.width, 0.05*Screen.height}} then
		weapon=3
	end
	if Gui.Button{id = "Weapon4", text = "Goo", size = {0.1*Screen.width, 0.05*Screen.height}} then
		weapon=4
	end
	Gui.group.pop{}

end


function Game:hud()
	love.graphics.setColor(0,0,0)
	love.graphics.rectangle("fill", 0, 0.9*Screen.height, Screen.width, 0.1*Screen.height)
	love.graphics.setColor(255, 255, 255)
	love.graphics.print(nbWave, 0.05*Screen.width, 0.95*Screen.height)
	love.graphics.setColor(0, 255, 0)
	love.graphics.print(hp, 0.1*Screen.width, 0.95*Screen.height)
	love.graphics.setColor(255, 0, 0)
  	love.graphics.print(nbTurrets, 0.9*Screen.width, 0.95*Screen.height)
	love.graphics.setColor(100, 100, 100)
	love.graphics.print(nbBombs, 0.95*Screen.width, 0.95*Screen.height)
	love.graphics.setColor(255, 255, 0)
	love.graphics.print(gold, 0.2*Screen.width, 0.95*Screen.height)
	love.graphics.setColor(255, 255, 255)
	local x, y = love.mouse.getPosition() -- get the position of the mouse
   	love.graphics.print(x, 0.9*Screen.width, 0.05*Screen.height)
	love.graphics.print(y, 0.95*Screen.width, 0.05*Screen.height)
	love.graphics.setColor(200, 10, 150)
	love.graphics.print(Game:tableSize(foes), 0.3*Screen.width, 0.95*Screen.height)
	love.graphics.print(love.timer.getFPS(), 0.4*Screen.width, 0.95*Screen.height)
	love.graphics.setColor(0, 255, 0, 100)
	love.graphics.print(nbSnipers, 0.925*Screen.width, 0.95*Screen.height)
end



function Game:update(dt)
	if dt < 1/30 then
		love.timer.sleep(1/30 - dt)
	end
	Screen.width, Screen.height = love.window.getDimensions()
	if ready then
		Game:miaou(dt)
	else
		Game:menu()
	end

	if Game.lvl then
		if hp<=0 then
			dead=true
			--[[if Config.highscore < nbWave then
				love.filesystem.write("miaou", "Highscore: " .. nbWave) -- TODO: Add number of waves of previous lvl
				Config.highscore = nbWave -- Same here
			end--]]
		elseif Game:tableSize(foes)==0 then
			won=true
		end

		if #turrets==nbTurrets and #bombs==nbBombs and #snipers==nbSnipers and #goos==nbGoos and not shop then
			ready = true
		end
	end
	random = math.random(0,2)
	frame=frame+1
	if nbWave == 8 then
		done = true
		Gui.group.push{grow = "down", pos = {0.5*Screen.width,0.5*Screen.height}}
		Gui.Label{ text = "GG !"}
		if Gui.Button{id = "Replay", text="Changer de niveau ?"} then
			done = false
			Game.resume = false
			Gamestate.switch(Level)
		end
		Gui.group.pop{}
	end
end



function Game:draw()
	love.graphics.draw(bg_image, bg_quad, 0, 0)
	if not done then
		if shop then
			Game:shop()
		else
			Game:drawPath()
			Game:drawTurrets()
			Game:drawFoes()
 		end
		Game.resume = true
	else
		Config.lvl = Game.lvl+1
		love.filesystem.write("miaou", "lvl: " .. Config.lvl)
	end

	Game:hud()
	Gui.core.draw() --Draw Gui
end

return Game
