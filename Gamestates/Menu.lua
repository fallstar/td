--Gamestates/Menu.lua
--by Fallstar

local Menu = {}


function Menu:update(dt)

	Gui.group.push{grow = "down", pos = {0.3*Screen.width,0.1*Screen.height}}  --Gui group for Button
	Gui.Label{ text = "Pause"}
	if Game.resume then
		if Gui.Button{id = "Resume", text="Reprendre", size = {0.4*Screen.width, 0.2*Screen.height}} then
			Gamestate.switch(Game, true)	--If Button Game pressed, switch to Game Gamestate
		end
	else
		if Gui.Button{id = "Game", text="Jouer", size = {0.4*Screen.width, 0.2*Screen.height}}then
			Gamestate.switch(Level)
		end
	end
	if Gui.Button{id = "About", text="À propos", size = {0.4*Screen.width, 0.2*Screen.height}} then
		Gamestate.switch(About)	--If Button Game pressed, switch to About Gamestate
	end
	if Gui.Button{id = "Config", text="Option", size = {0.4*Screen.width, 0.2*Screen.height}} then
		Gamestate.switch(Config)
	end
	if Gui.Button{id = "Quit", text="Quitter", size = {0.4*Screen.width, 0.2*Screen.height}} then
		love.event.quit()
	end
	Gui.group.pop{}
end
	

function Menu:draw()
	Gui.core.draw() --Draw Gui
end



return Menu
