--Gamestates/About.lua
--by Fallstar

local About = {}

function About:enter()
	Previous = "Gamestates/Menu"
end

function About:update(dt)
		Gui.group.push{grow = "down", pos = {0.1*Screen.width,0.1*Screen.height}}
		Gui.Label{text = "*Nom du jeu*", size = {0.4*Screen.width, 0.1*Screen.height}}
		Gui.Label{text = "Un Tower Defense", size = {0.4*Screen.width, 0.1*Screen.height}}
		Gui.Label{text = "par Fallstar.", size = {0.4*Screen.width, 0.1*Screen.height}}
		Gui.Label{text = "D'après une idée de *pseudo de Nicolas*", size = {0.4*Screen.width, 0.1*Screen.height}}
		Gui.Label{text = "Remerciements :", size = {0.4*Screen.width, 0.1*Screen.height}}
		Gui.Label{text = "piernov", size = {0.4*Screen.width, 0.1*Screen.height}}

end 

function About:draw()
	Gui.core.draw()
end 


return About
