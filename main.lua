--main.lua
--by Fallstar
     
Gamestate = require "hump.gamestate"
Menu = require "Gamestates/Menu"
Game = require "Gamestates/Game"
About = require "Gamestates/About"
Config = require "Gamestates/Config"
Level = require "Gamestates/Level"
Gui = require "Quickie"

Screen = {} -- Contains window width and height


function love.load()
	Screen.width, Screen.height = love.window.getDimensions()
	Gamestate.registerEvents()
	Gamestate.switch(Menu)
	love.graphics.setFont(love.graphics.newFont(14*(1/800)*Screen.width))
	Config:loadUserConfig() 

end




function love.keypressed(key)
	if key == "escape" then -- Handle return key on Android or Escape key on regular keyboard
		if Previous then -- Switch to previous entry if we aren't a the menu's root, otherwise quit
			Gamestate.switch(require(Previous))
		else
			love.event.quit()
		end
	end
end
